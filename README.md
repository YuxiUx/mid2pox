# mid2pox
Simple command line tool for converting midi files to pox.  
_**Pox** is fileformat used by **Ponyxis tracker** - builtin music tracker on **PonyxisOS**_  
  
[Get executable](https://bitbucket.org/YuxiUx/mid2pox/downloads/)

## usage 
`usage: mid2pox [-h] [-N] [-S] midifile.mid [midifile.mid ...] [-o out.pox [out.pox ...]]`  
### arguments:
  `midifile.mid` one or more input files _(required)_   
  `-o out.pox [out.pox ...]`, `--output out.pox [out.pox ...]` Output filename(s). If not specified outputs be named same as original file _(optional)_
### switches: _(optional)_
  `-h`, `--help` show help message and exit  
  `-N`, `--noask` Overwrite exisitng pox files without asking  
  `-S`, `--skiperror` Skip invalid/corupted files (in default program stop)
### return codes:
 - 0 - everiting converted successfully (or when option `--skiperror` is used)  
 - 1 - one or more input files does not exist  
 - 2 - one or more input files is corupted  

## dependences
- python3
- mido

## known issues
- well nothing? 

## Compilation
if you want to compile single python executable file (on linux)  
```
bash build-singlefile.sh
```
output file can be executed (on linux, mac)
```
./mid2pox 
```
windows 
```
mid2pox
```
Or longer but universal way (+ fallback)
```
python mid2pox 
```
This is not native executable. Still require python3 and all dependences 
to be installed.  

