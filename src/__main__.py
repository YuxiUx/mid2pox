#!/usr/bin/python

# code by YuxiUx
# https://bitbucket.org/YuxiUx/

import cli
import utils as u
import options as opt
import midiconvert as mp

opt.init()

def main():
    args = cli.getParams()
    opt.NO_ASK = args.noask
    opt.SKIP_ERROR = args.skiperror
    ins = args.input
    out = u.getOuts(args.input, args.output)
    for k, file in enumerate(ins):
        print("[",k+1,"/",len(ins),"]","\tConverting", file)
        mp.convertToMidi(file, out[k])




if __name__ == "__main__":
    main()