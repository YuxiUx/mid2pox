# code by YuxiUx
# https://bitbucket.org/YuxiUx/

import re
import cli
import os.path
import options as opt
from query_yes_no import query_yes_no as query

def generateFilename(name):
    return re.sub('\.midi*$', '', name) + '.pox'

def getOuts(ins,out):
    if(out==None):
        out=[]
    CheckFiles(ins)
    for i in range(0, len(ins)):
        ins[i] = ins[i].strip()
        if(i>=len(out)):
            out.append(generateFilename(ins[i]))
        out[i] = out[i].strip()
        print("Asigning output:", ins[i], "to", out[i])
    CheckFiles(out, False, True)
    return out          
  
def CheckFiles(files, fail=True, ask=False):
    globfail = False
    for k, file in enumerate(files):
        f = os.path.isfile(file)
        if(fail and not f):
            print("Error: File", file, "does not exist.")
            globfail = True
        if(ask and f and not opt.NO_ASK):
            files[k] = FileExistDialog(file)
    if(globfail and fail and not opt.SKIP_ERROR):
        print("Aborting")
        exit(1)
    return files

def FileExistDialog(file):
    while(os.path.isfile(file)):
        if(query("File "+ file+ " already exist. Do you want to overwrite it?") == "yes"):
            return file
        file = cli.RequiredInput()
    return file

