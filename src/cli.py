# code by YuxiUx
# https://bitbucket.org/YuxiUx/

import argparse

#CLI
def RequiredInput(title="Enter new filename: ", err="Filename can't be empty"):
    st = input(title).strip()
    while (st == ''):
        print(err)
        st = input(title).strip()
    return st


#PARAMS
def getParams():
    parser = argparse.ArgumentParser(description='Simple comand line tool for convert midi to pox.',
        epilog='Return codes: everithing ok (0), input file does not exist (1), input file is corupted (2)')
    parser.add_argument('input', metavar='midifile.mid', nargs="+",
        help='Midi file(s) to be converted')
    parser.add_argument('-o','--output', metavar='out.pox', nargs="+", 
        help='Output filename(s). If not specified outputs be named same as original file')
    parser.add_argument('-N','--noask', action='store_true',
        help='Overwrite exisitng pox files without asking')
    parser.add_argument('-S','--skiperror', action='store_true',
        help='Skip invalid/corupted files (in default program stop)')
    return parser.parse_args()


