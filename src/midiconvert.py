# code by YuxiUx
# https://bitbucket.org/YuxiUx/

import mido
from pox import pox
from os import system
import options as opt

def convertToMidi(inp, out):
    try:
        mid = mido.MidiFile(inp)
        totalmid = int(str(mid).split(", ")[2].split(" ")[0])
        
        p = pox()
        p.tpb = mid.ticks_per_beat
        p.channelcount = len(mid.tracks)
        
        for msg in mid:
            if(msg.type=="note_on"):
                p.Append(msg)
        p.Save(out)
        print("Conversion done. Saved as", out)
    except:
        print("Conversion Error: probably invalid or corupted midi file. (", inp,")")
        if(not opt.SKIP_ERROR):
            exit(2)
