class pox:
    """
        Pox file object
        Code by YuxiUx
        https://bitbucket.org/YuxiUx
    """
    def __init__(self, name="", tpb=90, channel_count=10, values=[]):
        self.name   = name
        self.tpb    = tpb
        self.values = values
        self.channelcount = channel_count
    def Append(self, line):
        if(isinstance(line, list)):
            if(len(line)==4):
                self.values.append(line)
            else:
                raise ValueError("Invalid format: list need 4 parameters")
        else:
            if(hasattr(line, 'channel')and hasattr(line, 'note')and hasattr(line, 'velocity')and hasattr(line, 'time')):
                self.Append([line.channel, line.note, line.velocity, line.time])
            else:
                raise ValueError("Invalid fomrat: dict/object must contain chnnel, note, velocity, time properties")
    def AppendPlain(self, channel, note, velocity, time):
        self.Append([channel, note, velocity, time])
    def Save(self, filename):
        file = open(filename, 'w')
        file.write("~ {} {}\n".format(self.tpb, self.channelcount))
        for val in self.values:
            if(val[3]%1==0):
                val[3] = int(val[3])
            file.write("{0[0]} {0[1]} {0[2]} {0[3]}\n".format(val))
        file.close()
    def Open(self, filename):
        file = open(filename, 'r')
        st = file.readline()
        self.values = []
        try:
            c, tpb, channelcount = st.split(' ')
            self.tpb = int(tpb)
            self.channelcount = int(channelcount)
            if(c!='~'):
                raise ValueError('Invalid start character')
            while(True):
                st = file.readline()
                if(st==""):
                    break
                a,b,c,d = st.split(' ')
                self.AppendPlain(int(a),int(b),int(c),float(d))
        except:
            raise ValueError('Corupted file')            
        file.close()








